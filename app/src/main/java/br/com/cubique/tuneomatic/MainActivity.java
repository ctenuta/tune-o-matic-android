package br.com.cubique.tuneomatic;

import android.app.Activity;
import android.graphics.Typeface;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.splunk.mint.Mint;

import java.util.Date;
import java.util.List;

import static java.util.Arrays.asList;


public class MainActivity extends Activity {

    private static final int RECORDER_SAMPLERATE = 44100;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;

    private final Handler handler = new Handler();
    int BufferElements2Rec = 2048; // want to play 2048 (2K) since 2 bytes we use only 1024
    int BytesPerElement = 2; // 2 bytes in 16bit format
    private AudioRecord recorder = null;
    private Thread recordingThread = null;



    private float pitchValid;

    private int currentAngle;
    private float lastAngle;


    float lastPitch;

    private boolean isPowered = false;
    private boolean isRecording = false;
    private boolean isPaused = false;
    private boolean isPitched = false;
    private float pitchProbability;

    private int lastNoteIndex;
    private Date lastPitchTime;

    private Typeface oswald;
    private Typeface montserrat;

    private RotateAnimation needleAnimation;
    private RotateAnimation arrowAnimation;

    ImageView needle;
    ImageView arrow;



    @Override
    protected void onCreate(Bundle savedInstanceState) {


        Mint.initAndStartSession(MainActivity.this, "57bd2d57");

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        super.onCreate(savedInstanceState);



        setContentView(R.layout.activity_main);


        oswald = Typeface.createFromAsset(getAssets(), "fonts/oswald_regular.ttf");
        montserrat = Typeface.createFromAsset(getAssets(), "fonts/montserrat_regular.otf");

        TextView textFrequency = (TextView) findViewById(R.id.frequency);
        TextView textCents = (TextView) findViewById(R.id.cents);
        TextView textNote = (TextView) findViewById(R.id.note);

        textNote.setTypeface(oswald);
        textFrequency.setTypeface(oswald);
        textCents.setTypeface(oswald);


        TextView labelButton = (TextView) findViewById(R.id.labelbutton);
        TextView labelCents = (TextView) findViewById(R.id.labelcents);
        TextView labelHz = (TextView) findViewById(R.id.labelhz);

        labelButton.setTypeface(oswald);
        labelHz.setTypeface(montserrat);
        labelCents.setTypeface(montserrat);

        setButtonHandlers();
        //enableButtons(false);

        lastAngle = 0;

        needle = (ImageView) findViewById(R.id.needle);
        arrow = (ImageView) findViewById(R.id.arrow);


        AdView mAdView = (AdView) findViewById(R.id.adView);
        mAdView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setVisibility(View.INVISIBLE);

        handler.postDelayed(Advertisement,30000);


    }

    @Override
    protected void onPause ()
    {

        isPaused = true;

        if (isRecording)
        {
            stopRecording();
            isRecording = true;
        }

        super.onPause();
    }

    @Override
    protected void onResume ()
    {



        super.onResume();

        isPaused = false;

        if (isRecording)
        {
            startRecording();
        }

    }



    private Runnable updateUI = new Runnable() {
        public void run() {



            if ((isRecording) &&  (isPitched))
            {


                float frequency = (float) 440.0;
                int A4_INDEX = 57;
                float r = (float) Math.pow(2.0, 1.0 / 12.0);
                float cent = (float) Math.pow(2.0, 1.0 / 1200.0);
                int r_index = 0;
                int cent_index = 0;


                if (pitchValid >= frequency) {
                    while (pitchValid >= r * frequency) {
                        frequency = r * frequency;
                        r_index++;
                    }

                    while (pitchValid > cent * frequency) {
                        frequency = cent * frequency;
                        cent_index++;
                    }

                    if ((cent * frequency - pitchValid) < (pitchValid - frequency))
                        cent_index++;

                    if (cent_index > 50) {
                        r_index++;
                        cent_index = 100 - cent_index;

                        if (cent_index != 0)
                            cent_index = cent_index * -1;
                    }
                } else {
                    while (pitchValid <= frequency / r) {
                        frequency = frequency / r;
                        r_index--;
                    }

                    while (pitchValid < frequency / cent) {
                        frequency = frequency / cent;
                        cent_index++;
                    }

                    if ((pitchValid - frequency / cent) < (frequency - pitchValid))
                        cent_index++;

                    if (cent_index >= 50) {
                        r_index--;
                        cent_index = 100 - cent_index;
                    } else {
                        if (cent_index != 0)
                            cent_index = cent_index * -1;
                    }

                }

                final int noteIndex = A4_INDEX + r_index;

                if ((noteIndex < 0) || (noteIndex > 119))
                    return;

                //Corrige o erro de salto de oitavas para frequências baixas
                if (noteIndex <= 30) {
                    lastNoteIndex = noteIndex;
                    lastPitchTime = new Date();
                } else {
                    if ((lastPitchTime != null) && (noteIndex > lastNoteIndex)) {

                        Date now = new Date();
                        int seconds = (int) (lastPitchTime.getTime() - now.getTime()) / (1000);

                        if (seconds <= 10) {
                            int difference = noteIndex - lastNoteIndex;

                            if (difference == 12)
                                return;
                        } else {
                            lastPitchTime = null;
                            lastNoteIndex = 0;
                        }

                    } else {
                        lastPitchTime = null;
                        lastNoteIndex = 0;
                    }

                }


                float ratio = (float) (noteIndex / 12.0);
                float aux = (float) (ratio - Math.floor(ratio));

                currentAngle = (int) (Math.round(aux * 12) * 30) + (360 * cent_index / 1200);


                String resultNote = noteNames.get(noteIndex);

                TextView textNote = (TextView) findViewById(R.id.note);
                textNote.setText(Html.fromHtml("<font size=\"14\">" + resultNote.substring(0,1) + "</font>" + "<font size=\"12\">" + resultNote.substring(resultNote.length() - 1) + "</font>"));

                ((TextView) findViewById(R.id.frequency)).setText(String.format("%.2f", pitchValid));

                ((TextView) findViewById(R.id.cents)).setText(cent_index > 0 ? String.format("+%d", cent_index) : String.format("%d", cent_index));



                if (lastAngle != currentAngle) {


                    needle.clearAnimation();
                    needleAnimation = new RotateAnimation(lastAngle, currentAngle, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
                    needleAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
                    needleAnimation.setDuration(1200);
                    needleAnimation.setFillEnabled(true);
                    needleAnimation.setFillAfter(true);

                    needle.setDrawingCacheEnabled(true);
                    needle.startAnimation(needleAnimation);



                    arrow.clearAnimation();
                    arrowAnimation = new RotateAnimation(lastAngle, currentAngle, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
                    arrowAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
                    arrowAnimation.setDuration(1200);
                    arrowAnimation.setFillEnabled(true);
                    arrowAnimation.setFillAfter(true);

                    arrow.setDrawingCacheEnabled(true);
                    arrow.startAnimation(arrowAnimation);

                    lastAngle = currentAngle;
                }

            }



            handler.postDelayed(this,1000);
        }

    };



    private List<String> noteNames = asList(
    "C0","C#0","D0","Eb0","E0","F0","F#0","G0","Ab0","A0","Bb0","B0",
    "C1","C#1","D1","Eb1","E1","F1","F#1","G1","Ab1","A1","Bb1","B1",
    "C2","C#2","D2","Eb2","E2","F2","F#2","G2","Ab2","A2","Bb2","B2",
    "C3","C#3","D3","Eb3","E3","F3","F#3","G3","Ab3","A3","Bb3","B3",
    "C4","C#4","D4","Eb4","E4","F4","F#4","G4","Ab4","A4","Bb4","B4",
    "C5","C#5","D5","Eb5","E5","F5","F#5","G5","Ab5","A5","Bb5","B5",
    "C6","C#6","D6","Eb6","E6","F6","F#6","G6","Ab6","A6","Bb6","B6",
    "C7","C#7","D7","Eb7","E7","F7","F#7","G7","Ab7","A7","Bb7","B7",
    "C8","C#8","D8","Eb8","E8","F8","F#8","G8","Ab8","A8","Bb8","B8",
    "C9","C#9","D9","Eb9","E9","F9","F#9","G9","Ab9","A9","Bb9","B9");


    private void setButtonHandlers() {


        RelativeLayout powerButton = (RelativeLayout) findViewById(R.id.powerbutton);

        final ImageView topPanel = (ImageView) findViewById(R.id.panel);
        final TextView labelButton = (TextView)findViewById(R.id.labelbutton);

        powerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isPowered = !isPowered;
             //   enableButtons(false);

                if (isPowered) {
                    topPanel.setImageResource(R.mipmap.toppanel_on);
                    labelButton.setText("ON");
                    startRecording();



                }
                else {
                    topPanel.setImageResource(R.mipmap.toppanel);
                    labelButton.setText("OFF");
                    stopRecording();
                    AdView mAdView = (AdView) findViewById(R.id.adView);
                    mAdView.setVisibility(View.VISIBLE);



                }

            }
        });
    }



    private Runnable Advertisement = new Runnable()
    {
        public void run() {

            AdView mAdView = (AdView) findViewById(R.id.adView);

            if (mAdView.getVisibility() == View.INVISIBLE)
            {
                mAdView.setVisibility(View.VISIBLE);
                handler.postDelayed(this, 120000);
            }
            else
            {
                mAdView.setVisibility(View.INVISIBLE);
                handler.postDelayed(this, 30000);
            }
        }
    };

    private void startRecording() {

        recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                RECORDER_SAMPLERATE, RECORDER_CHANNELS,
                RECORDER_AUDIO_ENCODING, BufferElements2Rec * BytesPerElement);


        try {
            recorder.startRecording();
        } catch (Exception e) {
            Log.e("Log:", "Exception, keep trying.", e);
        }

        isRecording = true;

        recordingThread = new Thread(new Runnable() {

            public void run() {
                grabAudioData();
            }
        }, "AudioRecorder Thread");

        recordingThread.start();



        handler.removeCallbacks(updateUI);
        handler.postDelayed(updateUI, 1000); // 1 second

    }


    private void stopRecording() {
        // stops the recording activity

        isRecording = false;

        if (null != recorder) {
            recorder.release();
            recorder = null;
            recordingThread = null;
        }

        handler.removeCallbacks(updateUI);


    }

    private void grabAudioData() {

        short sData[] = new short[BufferElements2Rec];

        while ((isRecording) && (!isPaused))
        {

            try
            {
                recorder.read(sData, 0, BufferElements2Rec);

                float[] sample = floatMe(sData);

                double soundPressureLevel = soundPressureLevel(sample);

                if (soundPressureLevel > 20)
                {
                    float pitch  =  getPitch(2048,44100,sample);

                    if ((isPitched) && (pitchProbability > 0.95))
                    {

                        lastPitch = pitchValid;
                        pitchValid = pitch;

                    }

                }
            }
            catch (Exception ex)
            {
                Log.d("MainActivity",ex.getMessage());
            }
        }
    }



    void difference (float inputBuffer[], float yinBuffer[], int bufferSize){
        int bufferSize2 = (int) bufferSize/2;
        int j, tau;
        float delta;

        for (tau = 0; tau < bufferSize2; tau++) {
            yinBuffer[tau] = 0;
        }
        for (tau = 1; tau < bufferSize2; tau++) {
            for (j = 0; j < bufferSize2; j++) {
                delta = inputBuffer[j] - inputBuffer[j+tau];
                yinBuffer[tau] += delta * delta;
            }
        }
    }

    void cummulativeMeanNormalizedDifference(float yinBuffer[], int bufferSize){
        int bufferSize2 = (int) bufferSize/2;
        int tau;

        yinBuffer[0] = 1;
        //Very small optimization in comparison with AUBIO
        //start the running sum with the correct value:
        //the first value of the yinBuffer

        float runningSum = yinBuffer[1];

        //yinBuffer[1] is always 1
        yinBuffer[1] = 1;

        //now start at tau = 2
        for (tau = 2; tau < bufferSize2; tau++) {
            runningSum += yinBuffer[tau];
            yinBuffer[tau] *= tau / runningSum;
        }
    }

    int absoluteThreshold(float yinBuffer[], int bufferSize){
        int bufferSize2 = (int) bufferSize/2;
        double threshold = 0.15;
        int tau;

        for (tau = 1; tau < bufferSize2; tau++){
            if  (yinBuffer[tau] < threshold) {
                while (tau+1 < bufferSize2 && yinBuffer[tau+1] < yinBuffer[tau]) tau++;

                pitchProbability = (1 - yinBuffer[tau]);
                isPitched = true;
                return tau;
            }
        }


        isPitched = false;
        pitchProbability = 0;
        return -1;
    }

    float parabolicInterpolation(int tauEstimate, float yinBuffer[], int bufferSize){
        int bufferSize2 = (int) bufferSize/2;
        float s0, s1, s2;
        int x0 = (tauEstimate < 1) ? tauEstimate : tauEstimate -1;
        int x2 = (tauEstimate + 1 < bufferSize2) ? tauEstimate + 1 : tauEstimate;
        if (x0 == tauEstimate)
            return (yinBuffer[tauEstimate] <= yinBuffer[x2]) ? tauEstimate : x2;
        if (x2 == tauEstimate)
            return (yinBuffer[tauEstimate] <= yinBuffer[x0]) ? tauEstimate : x0;
        s0 = yinBuffer[x0];
        s1 = yinBuffer[tauEstimate];
        s2 = yinBuffer[x2];
        return tauEstimate + 0.5f * (s2 - s0) / (2.0f * s1 - s2 - s0);
    }

    float getPitch(int inNumberFrames, float sampleRate, float inData[]){

        pitchProbability = 0;
        int bufferSize = inNumberFrames;

        float inputBuffer[] = new float[bufferSize];
        float yinBuffer[] = new float[bufferSize/2];

        int tauEstimate = -1;
        float pitchInHertz = 0;

        // Setp 0: Get the data
        for (int i = 0; i < bufferSize; i++) {
            inputBuffer[i] = inData[i]/(32768); // 2^15 = 32768
        }

        // Step 2: Difference
        difference(inputBuffer, yinBuffer, bufferSize);

        // Step 3: cumulativeMeanNormalizedDifference
        cummulativeMeanNormalizedDifference(yinBuffer, bufferSize);
        // The cumulative mean normalized difference function
        // as described in step 3 of the YIN paper [1]

        // Step 4: absoluteThreshold
        tauEstimate = absoluteThreshold(yinBuffer, bufferSize);

        // Step 5: parabolicInterpolation
        if (tauEstimate !=-1) {
            float betterTau = parabolicInterpolation(tauEstimate, yinBuffer, bufferSize);
            pitchInHertz = sampleRate/betterTau;
        }

        return pitchInHertz;
    }

    private double soundPressureLevel(final float[] buffer) {
        double power = 0.0D;
        for (float element : buffer) {
            power += element * element;
        }
        double value = Math.pow(power, 0.5) / buffer.length;
        return 20.0 * Math.log10(value);
    }

    public static float[] floatMe(short[] pcms) {
        float[] floaters = new float[pcms.length];
        for (int i = 0; i < pcms.length; i++) {
            floaters[i] = pcms[i];
        }
        return floaters;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
